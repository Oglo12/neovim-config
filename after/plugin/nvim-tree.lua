vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

local nvim_tree = require("nvim-tree")

nvim_tree.setup()

local all_width = vim.api.nvim_win_get_width(0)
local all_height = vim.api.nvim_win_get_height(0)

local width = math.floor(all_width / 2)
local height = math.floor(all_height / 1.5)

nvim_tree.setup({
    hijack_cursor = true,
    update_cwd = false,
    reload_on_bufenter = true,
    select_prompts = false,
    filesystem_watchers = {
        enable = true,
    },
    filters = {
        dotfiles = false,
    },
    view = {
        width = 30,
        side = "left",
        float = {
            enable = true,
            open_win_config = {
                border = "solid",
                width = width,
                height = height,
                row = math.floor((all_height - height) / 2),
                col = math.floor((all_width - width) / 2)
            },
        },
    },
    renderer = {
        icons = {
            show = {
                git = true,
            },
            glyphs = { --[[ :help nvim-tree (Line 470) ]]--
                default = "",
                symlink = "",
                bookmark = "󰆤",
                modified = "●",
                folder = {
                    arrow_closed = " ",
                    arrow_open = " ",
                    default = "",
                    open = "",
                    empty = "",
                    empty_open = "",
                    symlink = "",
                    symlink_open = "",
                },
                git = {
                    unstaged = "✗",
                    staged = "✓",
                    unmerged = "",
                    renamed = "➜",
                    untracked = "",
                    deleted = "",
                    ignored = "◌",
                },
            },
        },
    },
    actions = {
        open_file = {
            quit_on_open = true,
        },
        change_dir = {
            enable = true,
            global = false,
            restrict_above_cwd = false,
        },
    },
})

vim.keymap.set("n", "<leader>pt", "<cmd>NvimTreeToggle<CR>")
