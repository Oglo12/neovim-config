function color_stuff(color)
	color = color or "everforest"

	vim.cmd.colorscheme(color)
end

color_stuff()
