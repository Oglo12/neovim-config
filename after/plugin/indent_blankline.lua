local ibl = require("ibl")

ibl.setup()

ibl.overwrite {
    exclude = {
        buftypes = {
            "nofile",
            "terminal",
        },
        filetypes = {
            "help",
            "dashboard",
            "NvimTree",
            "TelescopePrompt",
            "undotree",
        },
    },
}
