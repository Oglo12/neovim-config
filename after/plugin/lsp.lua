local lsp = require("lsp-zero")
local lspconfig = require("lspconfig")

local must_be_installed = {
	"rust_analyzer", -- Rust
	"lua_ls", -- Lua
    "taplo", -- TOML
    "clangd", -- C/C++
}

lsp.preset("recommended")

require('mason').setup({})
require('mason-lspconfig').setup({
	ensure_installed = must_be_installed,
	handlers = {
		function(server_name)
			require('lspconfig')[server_name].setup({})
		end,
	},
})

lsp.on_attach(function(client, bufnr)
	local opts = { buffer = bufnr, remap = false }

	vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
	vim.keymap.set("n", "<C-k>", function() vim.lsp.buf.hover() end, opts)
end)

lsp.setup()
