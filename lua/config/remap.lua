-- Random Keybindings --
vim.keymap.set("n", "<C-s>", ":w<CR>")

-- Move Selected Content --
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Open Buffers
vim.keymap.set("n", "<leader>wv", ":vnew<CR>")
vim.keymap.set("n", "<leader>wh", ":new<CR>")
vim.keymap.set("n", "<leader>wV", ":vsplit<CR>")
vim.keymap.set("n", "<leader>wH", ":split<CR>")

-- Open Tabs
vim.keymap.set("n", "<leader>tn", ":tabnew | NvimTreeOpen<CR>")
vim.keymap.set("n", "<leader>tc", ":tabclose<CR>")
