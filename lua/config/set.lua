--[[ SETTINGS ]]--
local insert_fat_cursor = false
local number_mode = 1 -- (0 = None, 1 = Line Numbers, 2 = Relative Line Numbers)
local tab_size = 4
local line_wrapping = false
local scrolloff = 8
local mapleader = " "
local column_marker = true
local column_marker_pos = 80
local remember_undo_changes = false



--[[ CODE ]]--
local function message(msg)
	print("[config.set]: " .. msg)
end

if (insert_fat_cursor) then
	vim.opt.guicursor = ""
end

if (number_mode == 1) then
	vim.opt.nu = true;
	vim.opt.relativenumber = false;
elseif (number_mode == 2) then
	vim.opt.nu = true;
	vim.opt.relativenumber = true;
elseif (number_mode == 0) then
	vim.opt.nu = false;
	vim.opt.relativenumber = false;
else
	message("Invalid value for 'number_mode': " .. number_mode .. " (Valid: 0, 1, 2)")
end

vim.opt.tabstop = tab_size
vim.opt.softtabstop = tab_size
vim.opt.shiftwidth = tab_size
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.wrap = line_wrapping

vim.opt.swapfile = false
vim.opt.backup = false
if (remember_undo_changes) then
    vim.opt.undodir = os.getenv("HOME") .. "/.cache/nvim/undodir"
    vim.opt.undofile = true
else
    vim.opt.undodir = os.getenv("HOME") .. "/.cache/nvim/nulldir"
    vim.opt.undofile = false
end

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = scrolloff
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

if (column_marker) then
    vim.opt.colorcolumn = "" .. column_marker_pos
end

vim.g.mapleader = mapleader
