function file_exists(path)
    local f = io.open(path, "r")

    if f ~= nil then
        io.close(f)

        return true
    else
        return false
    end
end

function read_file(path)
    local file = io.open(path, "r")

    if not file then
        return nil
    end

    local content = file:read("*a")
    file:close()

    return content
end

function update_config()
    os.execute("cd ~/.config/nvim && git pull origin main > /tmp/nvupout 2>&1 || touch /tmp/nvim_failed_update")

    local status = not file_exists("/tmp/nvim_failed_update")

    local output = read_file("/tmp/nvupout")

    if (status == false) then
        vim.notify("Failed to update configuration! (T~T)\n\n" .. output, vim.log.levels.ERROR)
    else
        vim.notify("Successfully updated configuration! (^-^)", vim.log.levels.INFO)
    end

    if file_exists("/tmp/nvim_failed_update") then
        os.remove("/tmp/nvim_failed_update")
    end

    if file_exists("/tmp/nvupout") then
        os.remove("/tmp/nvupout")
    end
end
