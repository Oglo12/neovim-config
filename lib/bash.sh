#!/usr/bin/env bash

export PATH=$HOME/.config/nvim/scripts:$PATH

nvim-delete-data () {
    rm -rf ~/.local/share/nvim || return 1
}

nvim-update-config () {
    begin_dir="$(pwd)"

    cd ~/.config/nvim

    if git pull origin main; then
        echo " "
        echo "Success! Neovim configuration is now up-to-date!"
    else
        echo "Failed to update Neovim configuration!" >&2

        cd "$begin_dir"

        return 1
    fi

    cd "$begin_dir"
}
